<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';

    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'begins_at',
        'ends_at',
        'price',
        'actual',
    ];

    protected $dates = [
        'begins_at',
        'ends_at',
    ];
}
